# README projeta

> *Documentation écrite le 12/10/2022*
### Description
Ce projet permet de monter deux serveurs web, avec un pare-feu pour chacun :
- Un serveur qui va héberger un site php
- Un serveur qui va héberger une api flask

Nous allons utiliser l'orchestrateur ansible qui utilisera :
- Trois rôles pour gérer l'installation et la configuration :
	*Du pare-feu ufw.
	*Du serveur http NGINX.
	*Du serveur api Flask-uwsgi-gunicorn.
- Un fichier inventaire en YAML.
- Une variable stocké dans un group_vars spécifiant l'utilisateur, pour établir les connexions SSH via ansible.
- Un playbook regroupant tous les rôles pour exécuter l'installation du projet en une seule traite.
- Tous les roles gèrent l'idempotence

### Prérequis connaissances
Infra
Linux
Bash
Python
Vagrant
Git

### Ressources utilisés
##### OS

> 1 WSL:  Debian 11 (bullseye) 
> 2 VM Vagrant: Debian 9 (stretch)

##### Logiciels

> (Windows) Vagrant 2.3 
> WSL 2 
> (WSL) Ansible 2.10.8
> (WSL) Python 3.9.2

##### Réseau "(hostname) : (ip)"

Réseau privé d'hôte Virtualbox (192.168.0.1)
> projeta-web.tk : 192.168.0.10 
> projeta-back.tk : 192.168.0.11

Forward du port 22 (vm) vers le port 2222 (hôte)

### WSL
#### Ansible
##### Schéma /etc/ansible
.
├── **00_inventory.yml** : Fichier d'inventaire
├── **ansible.cfg** : Fichier de configuration
├── group_vars : Contient les variables lié aux **groupes**
├── host_vars : Contient les variables lié aux **hôtes**
├── **playbook** : Contient les playbook pour rouler les roles
├── **roles** : Contient les roles ansible
└── ssh.sh : Script non indempotent pour gérer les connexions

Nom ansible des hôtes:
front : 192.168.0.10 (projeta_web)
back : 192.168.0.11 (projeta_back)

La connexion SSH, "ansible-controller" vers machines hôtes se fait avec l'utilisateur "vagrant". (voir la variable dans le fichier group_vars/**all.yml**)

### Vagrant
Vagrantfile.rb : Fichier Vagrant contenant le script de montage des VM.
Il monte deux VM appelé "projeta_web" et "projeta_back")

### Machines virtuelles
#### front (projeta_web)
Contient l'index.php

#### back (projeta_back)
Contient l'api flask

#### Raccourci vers les README
* [Vagrant](URL)
* [SSH.SH](URL)
* [ROLE ANSIBLE](URL)
* [ROLE FLASK-UWSGI](URL)
* [ROLE NGINX](URL)
* [ROLE UFW](URL)


### Documentation
* [Documentation sur les modules Ansible](https://docs.ansible.com/ansible/2.9/modules/list_of_all_modules.html)

* [Source Installation Nginx](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-debian-9/)

* [Source Configuration Nginx & USWGI](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04)
