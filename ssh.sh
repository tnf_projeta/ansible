#!/bin/bash
#Script de création de connexion SSH via WSL

#VARS
DIR=$HOME/.ssh
USERNAME="vagrant"
server=IP      # server IP
port=22                 # port
connect_timeout=5       # Connection timeout

#DICTIONNARY
declare -a SERVERS=("front" "back")

for server in ${SERVERS[@]}; do
        if [ -d "$DIR" ]; then
                if [ "$(ls -A $DIR)" ]; then
                        echo "$DIR contient un ou plusieurs fichiers"
                        echo "##################################"
                        echo "Vérification de la connexion SSH"
                        timeout 5 bash -c "</dev/tcp/$server/$port"
                        if [ $? == 0 ]; then
                                echo "##################################"
                                echo "TEST CONNEXION VERS L'HOTE $server:$port"
                                echo "----------------------------------"
                                echo "Connexion à l'hôte $server: REUSSIE"
                                echo "----------------------------------"
                                echo "Aucune action n'est nécessaire..."
                                echo "Fin du Script !"
                        else
                                echo "##################################"
                                echo "TEST CONNEXION VERS L'HOTE $server:$port"
                                echo "----------------------------------"
                                echo "Connexion à l'hôte $server: K.O"
                                echo "----------------------------------"
                                # if grep -q 'All keys were skipped' /tmp/ssh_output.txt; then
                                #         echo 'SKIP'
                                # fi
                                echo "Fin du Script !"
                        fi
                else    
                        ssh-keygen -t ecdsa -b 521
                        for server in ${SERVERS[@]}; do             
                                echo "$DIR est vide"
                                ssh-copy-id $USERNAME@$server
                        done
                        echo "Ajout de l'agent ssh"
                        eval `ssh-agent`
                        echo "Ajout ssh-add"
                        ssh-add
                fi
        else
                ssh-keygen -t ecdsa -b 521
                for server in ${SERVERS[@]}; do             
                        echo "$DIR est vide"
                        ssh-copy-id $USERNAME@$server
                done
                echo "Ajout de l'agent ssh"
                eval `ssh-agent`
                echo "Ajout ssh-add"
                ssh-add
        fi
done